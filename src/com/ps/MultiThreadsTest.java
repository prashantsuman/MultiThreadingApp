package com.ps;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MultiThreadsTest {

	public static void main(String[] args) {
		System.out.println("Application Started....");
		LocalDateTime start = LocalDateTime.now();
		Info info = new Info();

		// Execution in sequential is very slow.
		// info.setInfo1(fetchInfo1());
		// info.setInfo2(fetchInfo2());
		// info.setInfo3(fetchInfo3());

		ExecutorService executor = Executors.newWorkStealingPool();

		List<Callable<String>> callables = Arrays.asList(() -> fetchInfo1("REQ1"), () -> fetchInfo2("REQ2"),
				() -> fetchInfo3("REQ3"));

		try {
			executor.invokeAll(callables).stream().map(future -> {
				try {
					return future.get();
				} catch (Exception e) {
					throw new IllegalStateException(e);
				}
			}).forEach(result -> {
				// Here based on response Id we can map the output
				System.out.println(result);
				String[] response = result.split(":");
				switch (response[0]) {
				case "REQ1":
					info.setInfo1(response[1]);
					break;
				case "REQ2":
					info.setInfo2(response[1]);
					break;
				case "REQ3":
					info.setInfo3(response[1]);
					break;
				}
			});
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		awaitTerminationAfterShutdown(executor);

		LocalDateTime end = LocalDateTime.now();
		Duration duration = Duration.between(start, end);

		assertTrue(duration.getSeconds() < 4.1);
		assertEquals("Info1", info.getInfo1());
		assertEquals("Info2", info.getInfo2());
		assertEquals("Info3", info.getInfo3());
	}

	private static String fetchInfo1(String requestID) {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return requestID + ":Info1";
	}

	private static String fetchInfo2(String requestID) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return requestID + ":Info2";
	}

	private static String fetchInfo3(String requestID) {
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return requestID + ":Info3";
	}

	static class Info {
		String info1;
		String info2;
		String info3;

		public String getInfo1() {
			return info1;
		}

		public void setInfo1(String info1) {
			this.info1 = info1;
		}

		public String getInfo2() {
			return info2;
		}

		public void setInfo2(String info2) {
			this.info2 = info2;
		}

		public String getInfo3() {
			return info3;
		}

		public void setInfo3(String info3) {
			this.info3 = info3;
		}
	}

	public static void awaitTerminationAfterShutdown(ExecutorService threadPool) {
		threadPool.shutdown();
		try {
			if (!threadPool.awaitTermination(5, TimeUnit.SECONDS)) {
				threadPool.shutdownNow();
			}
		} catch (InterruptedException ex) {
			threadPool.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}